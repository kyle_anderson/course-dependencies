package models

type JsonLoadable interface {
	LoadFromJson([]byte) error
}

type JsonSerializable interface {
	ToJson() ([]byte, error)
}

type Course interface {
	JsonLoadable
	JsonSerializable

	Description() string
	SetDescription(string)
	Department() string
	SetDepartment(string)
	Code() uint
	SetCode(uint)

	Save() error
	Delete() error
}

type CourseData struct {
	Description string
	/* Department portion of course identifier. If the identifier
	is "ECE 405" then the department is "ECE" */
	Department string
	/* Course Code portion. If the course identifier is "ECE 405",
	then the code is "405" */
	Code uint
}

type BaseCourse struct {
	Data CourseData
}

func (c BaseCourse) Description() string                { return c.Data.Description }
func (c BaseCourse) Department() string                 { return c.Data.Department }
func (c BaseCourse) Code() uint                         { return c.Data.Code }
func (c *BaseCourse) SetDescription(description string) { c.Data.Description = description }
func (c *BaseCourse) SetDepartment(department string)   { c.Data.Department = department }
func (c *BaseCourse) SetCode(code uint)                 { c.Data.Code = code }
