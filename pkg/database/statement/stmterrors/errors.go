package stmterrors

import (
	"errors"
	"fmt"
)

type NoSuchStatementError struct {
	Id uint
}

func (err *NoSuchStatementError) Error() string {
	return fmt.Sprintf(`no statement with ID "%d" has been registered`, err.Id)
}

type FailedPrepareError struct {
	RawQuery     string
	PrepareError error
}

func (err *FailedPrepareError) Error() string {
	return fmt.Sprintf("Failed to prepare query: %s\nError: %v", err.RawQuery, errors.Unwrap(err))
}

func (err *FailedPrepareError) Unwrap() error { return err.PrepareError }
