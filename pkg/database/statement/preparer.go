package statement

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/statement/internal"
)

func NewRawPreparer(db database.DatabaseConnector, queries internal.Queries) Preparer {
	return Preparer{BasePreparer: internal.NewLazyPreparer(&internal.DBSource{DB: db, Queries: queries})}
}

func NewPreparerForTx(tx *sqlx.Tx, existingPreparer Preparer) Preparer {
	return Preparer{BasePreparer: internal.NewLazyPreparer(&internal.ReboundSource{Tx: tx, NestedSource: existingPreparer.Source()})}
}

type BasePreparer interface {
	Get(id uint) (*sqlx.Stmt, error)
	/* Frees up all resources created by the preparer. This does not include anything
	shared with it, such as a database connection or transaction. */
	Close()
	Source() internal.PrepareSource
}

type Preparer struct {
	BasePreparer
}

func (p *Preparer) GetOrPanic(id uint) *sqlx.Stmt {
	stmt, err := p.Get(id)
	if err != nil {
		panic(fmt.Sprintf(`unable to prepare statement with ID "%d". Error: %v`, id, err))
	}
	return stmt
}
