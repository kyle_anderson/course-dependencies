package internal

import (
	"fmt"
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" /* TODO not sure how to import this nicely such that `all` controls it */
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/all"
)

func TestLazyDBPreparer(t *testing.T) {
	for _, driverName := range all.AllDBDrivers() {
		var driverName = driverName
		t.Run(fmt.Sprintf("%s preparer test", driverName), func(t *testing.T) {
			t.Parallel()
			t.Logf("Driver: %s", driverName)
			statementsTable := []Queries{
				{0: `CREATE TABLE "some" (id int PRIMARY KEY)`},
				{0: `CREATE TABLE "some" (id int PRIMARY KEY)`, 2: `CREATE TABLE "another" (id int PRIMARY KEY)`},
			}
			for _, statements := range statementsTable {
				t.Run(fmt.Sprintf("WITH %v input statements", len(statements)), func(t *testing.T) {
					preparer := setupPreparer(driverName, statements, t)
					defer preparer.Close()
					preppedStmtsTable := make(map[uint]*sqlx.Stmt)
					t.Run("WHEN getting a valid statement which hasn't been prepared before "+
						"THEN the statement is prepared and added to the cache", func(t *testing.T) {
						counter := 0
						for key := range statements {
							counter++
							var err error
							preppedStmt, err := preparer.Get(key)
							if err != nil || preppedStmt == nil {
								t.Fatalf("Failed to prepare query: Error: %v, Stmt: %v", err, preppedStmt)
							} else if receivedLen := len(preparer.preparedQueries); receivedLen != counter {
								t.Fatalf("preparedQueries size mismatch after running a new query. "+
									"Expected size: %v. Received size: %v", counter, receivedLen)
							}
							preppedStmtsTable[key] = preppedStmt
						}
					})
					t.Run("WHEN getting a statement ID which is out of range "+
						"THEN the operation returns an appropriate error, and the cache is untouched", func(t *testing.T) {
						startCacheLength := len(preparer.preparedQueries)
						preppedStmt, err := preparer.Get(1)
						if err == nil || preppedStmt != nil {
							t.Fatalf("Expected to fail retrieving test with ID 1, but got preppedStmt: %v, err: %v", preppedStmt, err)
						} else if newCacheLength := len(preparer.preparedQueries); newCacheLength != startCacheLength {
							t.Fatalf("Cache size changed after invalid query fetched. "+
								"Previous size: %v. New size: %v. cache: %#v",
								startCacheLength, newCacheLength, preparer.preparedQueries)
						}
					})
					t.Run("WHEN getting a statement which has previously been prepared "+
						"THEN the cache doesn't grow and the same statement as previously generated is fetched", func(t *testing.T) {
						lengthGoingIn := len(preparer.preparedQueries)
						for key, expectedStmt := range preppedStmtsTable {
							newPreppedStmt, err := preparer.Get(key)
							if err != nil || newPreppedStmt == nil {
								t.Fatalf("Failed to prepare query: Error: %v, Stmt: %v", err, newPreppedStmt)
							} else if newLen := len(preparer.preparedQueries); newLen != lengthGoingIn {
								t.Errorf("preparedQueries changed in size after fetching previously prepared query. "+
									"Size going in: %v. Final size: %v", lengthGoingIn, newLen)
							} else if newPreppedStmt != expectedStmt {
								t.Errorf("A prepared statement other than the cached one has been fetched. "+
									"Cached: %v (addr %p). Just fetched: %v (addr %p)", expectedStmt, &expectedStmt, newPreppedStmt, newPreppedStmt)
							}
						}
					})
				})
			}
		})
	}
}

func setupPreparer(driverName string, statements Queries, t *testing.T) *LazyPreparer {
	t.Logf("Setting up preparer with %v statements", len(statements))
	connInfo, err := all.FetchEnvConnInfo(driverName)
	if err != nil {
		t.Fatalf("Failed to fetch environment connection info for driver %s. Error: %v", driverName, err)
	}
	db, err := sqlx.Connect(driverName, connInfo)
	if err != nil {
		t.Fatalf("Failed to connect to database, error: %v", err)
	}
	source := DBSource{DB: db, Queries: statements}
	preparer := NewLazyPreparer(&source)

	if len(preparer.preparedQueries) != 0 {
		t.Fatalf("preparedQueries is starting nonempty: %#v", preparer.preparedQueries)
	}
	return preparer
}
