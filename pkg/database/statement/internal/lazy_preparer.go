package internal

import (
	"log"

	"github.com/jmoiron/sqlx"
)

type LazyPreparer struct {
	preparedQueries map[uint]*sqlx.Stmt
	source          PrepareSource
}

func NewLazyPreparer(source PrepareSource) *LazyPreparer {
	return &LazyPreparer{source: source, preparedQueries: make(map[uint]*sqlx.Stmt)}
}

func (p *LazyPreparer) Get(id uint) (*sqlx.Stmt, error) {
	var stmt *sqlx.Stmt = nil
	var err error = nil
	stmt, ok := p.preparedQueries[id]
	if !ok {
		stmt, err = p.Source().Prepare(id)
		if err == nil {
			if stmt == nil {
				log.Panicf("Received nil statement back from PrepareSource for statement %v", id)
			}
			p.preparedQueries[id] = stmt
		}
	}
	return stmt, err
}

func (p *LazyPreparer) Source() PrepareSource { return p.source }

func (p *LazyPreparer) Close() {
	/* We don't close any PrepareSources, as those aren't supposed to hold anything,
	and if a nested PrepareSource holds something, then that is a shared resource over which
	we don't have ownership. */
	for _, stmt := range p.preparedQueries {
		if stmt != nil {
			stmt.Close()
		} else {
			log.Panic("Somehow obtained nil statement in LazyPreparer.Close()")
		}
	}
	p.preparedQueries = make(map[uint]*sqlx.Stmt)
}
