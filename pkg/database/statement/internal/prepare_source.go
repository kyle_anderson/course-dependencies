package internal

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/statement/stmterrors"
)

type PrepareSource interface {
	Prepare(uint) (*sqlx.Stmt, error)
}

type Queries = map[uint]string

type DBSource struct {
	DB database.DatabaseConnector
	Queries
}

func (source *DBSource) Prepare(id uint) (*sqlx.Stmt, error) {
	var err error
	var stmt *sqlx.Stmt
	rawStmt, ok := source.Queries[id]
	if !ok {
		err = &stmterrors.NoSuchStatementError{Id: id}
	} else {
		stmt, err = source.DB.Preparex(source.DB.Rebind(rawStmt))
		if err != nil {
			err = &stmterrors.FailedPrepareError{RawQuery: rawStmt, PrepareError: err}
		}
	}
	return stmt, err
}

/* A query preparer which rebinds queries prepared by another source. */
type ReboundSource struct {
	NestedSource PrepareSource
	Tx           *sqlx.Tx
}

func (source *ReboundSource) Prepare(id uint) (*sqlx.Stmt, error) {
	stmt, err := source.NestedSource.Prepare(id)
	if err != nil {
		return nil, err
	}
	stmt = source.Tx.Stmtx(stmt)
	return stmt, err
}
