package database

import "github.com/jmoiron/sqlx"

type DatabaseConnector interface {
	sqlx.Preparer
	sqlx.PreparerContext
	Preparex(query string) (*sqlx.Stmt, error)
	Rebind(query string) string
}
