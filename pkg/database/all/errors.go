package all

import "fmt"

type ConnInfoUnsetError struct {
	DriverName string
}

func (err ConnInfoUnsetError) Error() string {
	return fmt.Sprintf("connection information for driver \"%s\" is unset", err.DriverName)
}
