package all

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql/pgconstants"
)

/* Gets the driver names of all supported drivers. */
func AllDBDrivers() []string {
	return []string{pgconstants.DriverName}
}

/* Maps a driver name to the name of the environment variable whose value
contains the connection information for the database.
Parameters:
	driverName: The name of the driver for which the environment variable should be fetched.
Returns:
	The name of the environment variable whose value contains the connection info for this
	database type.
*/
func EnvVariableFor(driverName string) string {
	driverName = strings.ReplaceAll(strings.ReplaceAll(driverName, "-", "_"), " ", "_")
	driverName = strings.ToUpper(driverName)
	return fmt.Sprintf("%s_DB_CONN_INFO", driverName)
}

/* Attempts to fetch the connection information for the database with the given driver name
from the corresponding environment variable.
Parameters:
	driverName: The name of the driver for which the information should be gathered.
Returns:
	1: The connection information placed in the driver's corresponding environment variable.
	This value is unspecified if there is an error.
	2: Any errors that occurred.
*/
func FetchEnvConnInfo(driverName string) (string, error) {
	connInfo := os.Getenv(EnvVariableFor(driverName))
	if len(connInfo) <= 0 {
		return "", ConnInfoUnsetError{driverName}
	}
	return connInfo, nil
}
