package database

import (
	"fmt"

	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

type Creator interface {
	NewCourse() models.Course
}

type Lister interface {
	ListCourses() (BaseCourseIterator, error)
}

type BaseHandler interface {
	Creator
	Lister

	CreateMigrator() (Migrator, error)
	Close()
}

type Handler struct {
	BaseHandler
}

func (h *Handler) ListCourses() (CourseIterator, error) {
	it, err := h.BaseHandler.ListCourses()
	if err != nil {
		return CourseIterator{}, err
	}
	return CourseIterator{BaseCourseIterator: it}, nil
}

func ConnectHandler(driverName string, connectionDetails string) (Handler, error) {
	handlerSupplier, ok := allHandlers[driverName]
	if !ok {
		return Handler{}, fmt.Errorf("no handler available for driver \"%s\"", driverName)
	}
	handler, err := handlerSupplier(driverName, connectionDetails)
	if err != nil {
		return Handler{}, err
	}
	return Handler{BaseHandler: handler}, nil
}

type HandlerSupplier = func(driverName string, connectionDetails string) (BaseHandler, error)

var allHandlers = make(map[string]HandlerSupplier)

func RegisterHandler(handlerSupplier HandlerSupplier, driverNames ...string) {
	for _, driverName := range driverNames {
		allHandlers[driverName] = handlerSupplier
	}
}
