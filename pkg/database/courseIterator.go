package database

import (
	"log"

	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

type BaseCourseIterator interface {
	Next() bool
	Current() (models.Course, error)
	Close()
}

type CourseIterator struct {
	BaseCourseIterator
}

const courseBufferSize = 20

func (it *CourseIterator) Stream(done <-chan struct{}) <-chan models.Course {
	output := make(chan models.Course, courseBufferSize)
	go func() {
		defer func() {
			close(output)
			it.Close()
		}()
		for it.Next() {
			course, err := it.Current()
			if err != nil {
				log.Printf("Failed to get course, error: %v", err)
				continue
			}
			select {
			case <-done:
				return
			case output <- course:
			}
		}
	}()
	return output
}
