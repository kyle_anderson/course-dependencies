package database

type Migrator interface {
	HasPendingMigrations() (bool, error)
	MigrateToLatest() error
	Close()
}
