package models

import "time"

type Record struct {
	Data RecordData
}

type RecordData struct {
	Id                   uint64
	CreatedAt, UpdatedAt time.Time
}

func (r Record) Id() uint64           { return r.Data.Id }
func (r Record) CreatedAt() time.Time { return r.Data.CreatedAt }
func (r Record) UpdatedAt() time.Time { return r.Data.UpdatedAt }
