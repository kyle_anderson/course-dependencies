package models

import (
	"encoding/json"

	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

type DBCourse struct {
	models.BaseCourse
	Record
}

func (c DBCourse) Data() DBCourseData {
	return DBCourseData{
		CourseData: c.BaseCourse.Data,
		RecordData: c.Record.Data,
	}
}

func (c *DBCourse) UpdateData(data DBCourseData) {
	c.BaseCourse.Data = data.CourseData
	c.Record.Data = data.RecordData
}

func (c *DBCourse) LoadFromJson(jsonData []byte) error {
	return json.Unmarshal([]byte(jsonData), &c.BaseCourse.Data)
}

func (c *DBCourse) ToJson() ([]byte, error) {
	return json.Marshal(c.BaseCourse.Data)
}

type DBCourseData struct {
	models.CourseData
	RecordData
}
