package postgresql

const (
	CreateCourse = iota
	UpdateCourse = iota
	DeleteCourse = iota
	ListCourses  = iota
)
