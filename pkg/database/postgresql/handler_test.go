package postgresql

import (
	"fmt"
	"testing"

	"gitlab.com/kyle_anderson/course-dependencies/internal"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

func withRollback(block func(*pgTxHandler)) error {
	handler, err := ConnectHandler(internal.GetDatabaseUrl())
	if err != nil {
		return fmt.Errorf("Failed to connect to database. Error: %w", err)
	}
	tx, err := handler.BeginTx()
	if err != nil {
		return fmt.Errorf("Failed to begin transaction. Error: %w", err)
	}
	defer tx.Rollback()
	block(tx)
	return nil
}

func TestListCourses(t *testing.T) {
	checkCleanAndExists := func(c *pgCourse) {
		if c.IsDirty || !c.Exists {
			t.Errorf("Course %d should be marked clean and exist, but one "+
				"of these wasn't met. IsDirty: %t, Exists: %t",
				c.Id(), c.IsDirty, c.Exists)
		}
	}

	err := withRollback(func(tx *pgTxHandler) {
		const numCourses = 5
		courses := make(map[uint64]*pgCourse, numCourses)
		departmentPool := []string{"TRON", "ECE", "CS", "SE", "MATH", "NE"}
		for i := uint(0); i < numCourses; i++ {
			newCourse := tx.NewPgCourseFromData(
				models.CourseData{
					Description: fmt.Sprintf("Course %d", i),
					Code:        i + 1,
					Department:  departmentPool[i%uint(len(departmentPool))],
				},
			)
			if err := newCourse.Save(); err != nil {
				t.Fatalf("Failed to save course. Error: %v", err)
			}
			checkCleanAndExists(newCourse)
			courses[newCourse.Id()] = newCourse
		}
		it, err := tx.ListPgCourses()
		if err != nil {
			t.Fatalf("Failed to list courses. Error: %v", err)
		}
		for it.Next() {
			receivedCourse, err := it.PgCurrent()
			checkCleanAndExists(receivedCourse)
			if err != nil {
				t.Fatalf("Error while getting PgCurrent: %v", err)
			}
			if expectedCourse, ok := courses[receivedCourse.Id()]; ok {
				// It's ok if the "list" action lists a course that we didn't create - there might be other stuff in the database.
				if *expectedCourse != *receivedCourse {
					t.Errorf("Received course with id %v is different from expected course. Received: %#v. Expected: %#v", receivedCourse.Id(), receivedCourse, expectedCourse)
				}
				delete(courses, receivedCourse.Id())
			}
		}
		if len(courses) != 0 {
			for _, course := range courses {
				t.Errorf("Expected to receive course with ID %d but never got it", course.Id())
			}
		}
	})
	if err != nil {
		t.Fatalf("withRollback error: %v", err)
	}
}
