package postgresql

import (
	dbmodels "gitlab.com/kyle_anderson/course-dependencies/pkg/database/models"
)

type pgCourse struct {
	dbmodels.DBCourse

	Handler         *pgHandler
	IsDirty, Exists bool
}

func NewCourse(handler *pgHandler) *pgCourse {
	return &pgCourse{Handler: handler, IsDirty: true, Exists: false}
}

func NewCourseFromData(handler *pgHandler, data dbmodels.DBCourseData) *pgCourse {
	course := NewCourse(handler)
	course.DBCourse.UpdateData(data)
	return course
}

func (c *pgCourse) Save() error {
	var err error
	var newCourse dbmodels.DBCourseData
	if c.IsDirty {
		if !c.Exists {
			newCourse, err = c.Handler.CreateCourse(c.DBCourse.Data())
		} else {
			newCourse, err = c.Handler.UpdateCourse(c.DBCourse.Data())
		}
		if err == nil {
			c.DBCourse.UpdateData(newCourse)
			c.IsDirty = false
			c.Exists = true
		}
	}
	return err
}

func (c *pgCourse) Delete() error {
	return c.Handler.DeleteCourse(c.Id())
}

func (c *pgCourse) markDirty() { c.IsDirty = true }
func (c *pgCourse) SetDescription(description string) {
	c.DBCourse.SetDescription(description)
	c.markDirty()
}
func (c *pgCourse) SetDepartment(department string) {
	c.DBCourse.SetDepartment(department)
	c.markDirty()
}
func (c *pgCourse) SetCode(code uint) {
	c.DBCourse.SetCode(code)
	c.markDirty()
}
func (c *pgCourse) LoadFromJson(jsonData []byte) error {
	err := c.DBCourse.LoadFromJson(jsonData)
	c.markDirty()
	c.Exists = false
	return err
}
func (c *pgCourse) ToJson() ([]byte, error) {
	return c.DBCourse.ToJson()
}
func (c *pgCourse) MarkCleanAndExists() {
	c.IsDirty = false
	c.Exists = true
}
