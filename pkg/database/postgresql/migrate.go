package postgresql

import (
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql/internal/migrate"
)

func (h *pgDBHandler) CreateMigrator() (database.Migrator, error) {
	return h.CreatePgMigrator()
}

func (h *pgDBHandler) CreatePgMigrator() (*migrate.Migrator, error) {
	return migrate.NewMigrator(h.DB)
}
