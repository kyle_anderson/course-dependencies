package postgresql

import (
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
	dbmodels "gitlab.com/kyle_anderson/course-dependencies/pkg/database/models"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql/pgconstants"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql/queries"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/statement"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

type pgHandler struct {
	preparer statement.Preparer
	DB       database.DatabaseConnector
	names    queries.SchemaNames
}

func init() {
	database.RegisterHandler(func(driver string, connectionDetails string) (database.BaseHandler, error) {
		if pgconstants.DriverName != driver {
			panic(fmt.Sprintf(`PostgreSQL handler given driver type "%s", should be "%s"`, driver, pgconstants.DriverName))
		}
		return ConnectHandler(connectionDetails)
	}, pgconstants.DriverName)
}

func ConnectHandler(connectionDetails string) (*pgDBHandler, error) {
	db, err := sqlx.Connect(pgconstants.DriverName, connectionDetails)
	if err != nil {
		return nil, err
	}
	return NewDBHandler(db), err
}

/* Creates a new PostgreSQL database handler, with the specified schema names.
You are responsible for closing the database connection,
which can be done with a call to `Close()`.
*/
func NewHandlerWithNames(db database.DatabaseConnector, names queries.SchemaNames) *pgHandler {
	return &pgHandler{DB: db, preparer: statement.NewRawPreparer(db, map[uint]string{
		CreateCourse: queries.CreateCourse(names),
		UpdateCourse: queries.UpdateCourse(names),
		DeleteCourse: queries.DeleteCourse(names),
		ListCourses:  queries.ListCourses(names),
	}), names: names}
}

func (h *pgHandler) SchemaNames() queries.SchemaNames { return h.names }

func (h *pgHandler) NewCourse() models.Course {
	return NewCourse(h)
}

func (h *pgHandler) NewPgCourseFromData(data models.CourseData) *pgCourse {
	return NewCourseFromData(h, dbmodels.DBCourseData{CourseData: data})
}

func (h *pgHandler) NewCourseFromData(data models.CourseData) models.Course {
	return h.NewPgCourseFromData(data)
}

func (h *pgHandler) CreateCourse(course dbmodels.DBCourseData) (dbmodels.DBCourseData, error) {
	log.Printf("Creating course: %v", course)
	return scanCourse(h.preparer.GetOrPanic(CreateCourse).QueryRowx(course.Description, course.Department, course.Code))
}

func (h *pgHandler) UpdateCourse(course dbmodels.DBCourseData) (dbmodels.DBCourseData, error) {
	log.Printf("Updating course: %v", course)
	return scanCourse(h.preparer.GetOrPanic(UpdateCourse).QueryRowx(course.Description, course.Department, course.Code))
}

func (h *pgHandler) ListCourses() (database.BaseCourseIterator, error) {
	return h.ListPgCourses()
}

func (h *pgHandler) ListPgCourses() (*CourseIterator, error) {
	rows, err := h.preparer.GetOrPanic(ListCourses).Queryx()
	if err != nil {
		return nil, err
	}
	return &CourseIterator{rows: rows, handler: h}, err
}

func (h *pgHandler) DeleteCourse(id uint64) error {
	_, err := h.preparer.GetOrPanic(DeleteCourse).Exec(id)
	return err
}

type scannable interface{ Scan(...interface{}) error }

func scanCourse(row scannable) (dbmodels.DBCourseData, error) {
	course := dbmodels.DBCourseData{}
	err := row.Scan(&course.Id, &course.CreatedAt, &course.UpdatedAt, &course.Description, &course.Department, &course.Code)
	return course, err
}
