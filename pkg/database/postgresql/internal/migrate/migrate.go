package migrate

import (
	"embed"
	"errors"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	pgmigrate "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/golang-migrate/migrate/v4/source/iofs"
	"github.com/jmoiron/sqlx"
)

const latestSchemaVersion = 3

type Migrator struct {
	migrator        *migrate.Migrate
	migrationSource source.Driver
}

func NewMigrator(db *sqlx.DB) (*Migrator, error) {
	migrator := Migrator{}
	err := prepareMigrator(db, &migrator)
	if err != nil {
		return nil, err
	}
	return &migrator, err
}

//go:embed migrations
var migrationsFs embed.FS

func prepareMigrator(db *sqlx.DB, m *Migrator) error {
	config := pgmigrate.Config{}
	pgDriver, err := pgmigrate.WithInstance(db.DB, &config)
	if err != nil {
		panic(fmt.Sprintf("failed to acquire database migration info. Error: %v", err))
	}
	m.migrationSource, err = iofs.New(migrationsFs, "migrations")
	if err != nil {
		return err
	}
	m.migrator, err = migrate.NewWithInstance("iofs", m.migrationSource, config.DatabaseName, pgDriver)
	return err
}

/* Determines if the database is up to date with all current migrations.
Preconditions: None
Postconditions: None
Returns:
	1: True if migrations are pending, false otherwise.
	It is of undefined value when the second value is present.
	2: Contains any errors that occurred.
*/
func (m *Migrator) HasPendingMigrations() (bool, error) {
	currentVersion, isDirty, err := m.migrator.Version()
	if err != nil {
		return true, err
	}
	return currentVersion < latestSchemaVersion || isDirty, nil
}

/* Migrates the database to the latest schema version.
   Preconditions: The migrator instance was created with no errors.
   Postconditions: The database is migrated to the latest supported schema, assuming no errors.
   Returns:
	1: Any errors that occur during migration. If an error occurs, it is guaranteed
	that the database will still be at a valid version and not dirty.
*/
func (m *Migrator) MigrateToLatest() error {
	currentVersion, isDirty, err := m.migrator.Version()
	if isDirty {
		/* The application should not leave the database in a dirty state after
		a migration, so if we find it in a dirty state, then we can't recover from it ourselves. */
		return errors.New("dirty database detected; cannot proceed with migrations")
	} else if err != nil {
		return err
	}
	for currentVersion < latestSchemaVersion {
		/* Unfortunately, golang-migrate doesn't support
		placing migrations in transactions. We however place all migration
		code in a transaction, so as long as we know the last successful migration
		version, we can force the version down to it if a migration fails.
		For this reason, we'll have to perform migrations in increments of one.
		*/
		err := m.migrator.Steps(1)
		if err != nil {
			m.migrator.Force(int(currentVersion))
			return err
		}
		currentVersion++
	}
	return nil
}

func (m *Migrator) Close() {
	m.migrationSource.Close()
}
