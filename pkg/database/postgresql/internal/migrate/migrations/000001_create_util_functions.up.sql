BEGIN;
CREATE FUNCTION update_created_at() RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
NEW.created_at = transaction_timestamp() AT TIME ZONE 'utc';
RETURN NEW;
END
$$;

CREATE FUNCTION update_updated_at() RETURNS TRIGGER LANGUAGE plpgsql AS $$
BEGIN
NEW.updated_at = transaction_timestamp() AT TIME ZONE 'utc';
RETURN NEW;
END
$$;
COMMIT;
