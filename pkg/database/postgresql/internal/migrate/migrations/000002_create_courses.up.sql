BEGIN;
CREATE TABLE courses (
    id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    description text,
    department varchar(5),
    code int CHECK (code > 0)
);

CREATE TRIGGER courses_updated_at BEFORE INSERT ON courses FOR EACH ROW EXECUTE FUNCTION update_created_at();
CREATE TRIGGER courses_created_at BEFORE INSERT OR UPDATE ON courses FOR EACH ROW EXECUTE FUNCTION update_updated_at();

COMMIT;
