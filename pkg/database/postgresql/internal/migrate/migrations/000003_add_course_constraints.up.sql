BEGIN;
ALTER TABLE courses 
    ADD CONSTRAINT unique_courses UNIQUE ("department", "description");
COMMIT;
