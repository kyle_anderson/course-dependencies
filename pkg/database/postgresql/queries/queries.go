package queries

func CreateCourse(names SchemaNames) string {
	const query = `
		INSERT INTO "{{.Courses.TableName}}" AS c 
		("{{.Courses.Description}}", "{{.Courses.Department}}", "{{.Courses.Code}}")
		VALUES (?, ?, ?)
		RETURNING "{{.IdCol}}", "{{.CreatedAtCol}}", 
			"{{.UpdatedAtCol}}", "{{.Courses.Description}}", 
			"{{.Courses.Department}}", "{{.Courses.Code}}"
	`
	return FillQuery(query, names)
}

func UpdateCourse(names SchemaNames) string {
	const query = `
		UPDATE "{{.Courses.TableName}}" SET 
		("{{.Courses.Description}}", "{{.Courses.Department}}", "{{.Courses.Code}}") 
		= 
		(?, ?, ?)
		WHERE "{{.IdCol}}" = ?
	`
	return FillQuery(query, names)
}

func DeleteCourse(names SchemaNames) string {
	const query = `
		DELETE FROM "{{.Courses.TableName}}" WHERE "{{.IdCol}}" = ?
	`
	return FillQuery(query, names)
}

func ListCourses(names SchemaNames) string {
	const query = `
		SELECT "{{.IdCol}}", "{{.CreatedAtCol}}", "{{.UpdatedAtCol}}", 
		"{{.Courses.Description}}", "{{.Courses.Department}}", "{{.Courses.Code}}"
		FROM "{{.Courses.TableName}}"
	`
	return FillQuery(query, names)
}
