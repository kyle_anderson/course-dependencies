package queries

type Courses struct {
	TableName   string
	Description string
	Department  string
	Code        string
}

type SchemaNames struct {
	IdCol, CreatedAtCol, UpdatedAtCol string
	Courses
}

func DefaultNames() SchemaNames {
	return SchemaNames{
		IdCol:        "id",
		CreatedAtCol: "created_at",
		UpdatedAtCol: "updated_at",
		Courses: Courses{
			TableName:   "courses",
			Description: "description",
			Department:  "department",
			Code:        "code",
		},
	}
}
