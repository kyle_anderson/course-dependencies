package queries

import (
	"strings"
	"text/template"
)

func FillQuery(query string, names SchemaNames) string {
	query = strings.ReplaceAll(strings.ReplaceAll(query, "\n", " "), "\t", "")
	tmpl, err := template.New("query").Parse(query)
	if err != nil {
		panic(err)
	}
	var writer strings.Builder
	err = tmpl.Execute(&writer, names)
	if err != nil {
		panic(err)
	}
	return writer.String()
}
