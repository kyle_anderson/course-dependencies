package postgresql

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

type CourseIterator struct {
	rows    *sqlx.Rows
	handler *pgHandler
}

func (it *CourseIterator) Next() bool {
	return it.rows.Next()
}

func (it *CourseIterator) Current() (models.Course, error) {
	return it.PgCurrent()
}

func (it *CourseIterator) PgCurrent() (*pgCourse, error) {
	c, err := scanCourse(it.rows)
	if err != nil {
		return nil, err
	}
	course := NewCourseFromData(it.handler, c)
	course.MarkCleanAndExists()
	return course, nil
}

func (it *CourseIterator) Close() { it.rows.Close() }
