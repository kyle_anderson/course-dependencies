package postgresql

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql/queries"
)

type pgDBHandler struct {
	DB *sqlx.DB
	*pgHandler
}

/* Creates a new PostgreSQL database handler.
You are responsible for closing the database connection,
which can be done with a call to `Close()`.
*/
func NewDBHandler(db *sqlx.DB) *pgDBHandler {
	return &pgDBHandler{DB: db, pgHandler: NewHandlerWithNames(db, queries.DefaultNames())}
}

func (h *pgDBHandler) Close() {
	h.preparer.Close()
	h.DB.Close()
}

func (h *pgDBHandler) BeginTx() (*pgTxHandler, error) {
	return TxHandlerFromDBHandler(h)
}
