package postgresql

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql/queries"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database/statement"
)

type pgTxHandler struct {
	Tx *sqlx.Tx
	*pgHandler
}

func NewTxHandler(tx *sqlx.Tx) *pgTxHandler {
	return &pgTxHandler{Tx: tx, pgHandler: NewHandlerWithNames(tx, queries.DefaultNames())}
}

func TxHandlerFromDBHandler(dbHandler *pgDBHandler) (*pgTxHandler, error) {
	tx, err := dbHandler.DB.Beginx()
	if err != nil {
		return nil, err
	}
	preparer := statement.NewPreparerForTx(tx, dbHandler.preparer)
	return &pgTxHandler{Tx: tx, pgHandler: &pgHandler{
		DB:       tx,
		preparer: preparer,
	}}, nil
}

func (handler *pgTxHandler) Commit() error {
	handler.close()
	return handler.Tx.Commit()
}

func (handler *pgTxHandler) Rollback() error {
	handler.close()
	return handler.Tx.Rollback()
}

/* Closes anything created by the Tx handler. */
func (handler *pgTxHandler) close() {
	handler.preparer.Close()
}
