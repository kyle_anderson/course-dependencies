package cli

import (
	"fmt"
	"log"

	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
)

func HandleMigrations(handler database.Handler) error {
	migrator, err := handler.CreateMigrator()
	if err != nil {
		return fmt.Errorf("failed to create migrator with error: %v", err)
	}
	defer migrator.Close()
	hasPendingMigrations, err := migrator.HasPendingMigrations()
	if err != nil {
		return fmt.Errorf("Failed to get migrations state. Error: %v\n", err)
	}
	log.Printf("Has pending migrations: %t", hasPendingMigrations)
	if hasPendingMigrations {
		log.Print("migrating to latest...")
		if err = migrator.MigrateToLatest(); err != nil {
			return fmt.Errorf("failed to migrate schema. Error: %w", err)
		}
	}
	return nil
}
