package cli

import "strings"

func TrimCmd(s string) string { return strings.Trim(s, " ") }
