package cli

import (
	"io"
	"log"
	"strings"

	"github.com/c-bata/go-prompt"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/models"
)

type Executor struct {
	Handler database.Handler
	out     io.Writer
}

func NewExecutor(handler database.Handler, out io.Writer) Executor {
	return Executor{Handler: handler, out: out}
}

func (e *Executor) Execute(cmd string) {
	cmd = TrimCmd(cmd)
	log.Printf(`Entering execute with cmd "%s"`, cmd)
	if strings.HasPrefix(cmd, createCmd+" ") {
		course := e.Handler.NewCourse()
		course.LoadFromJson([]byte(cmd[len(createCmd):]))
		course.Save()
		e.printCourse(course)
	} else if strings.HasPrefix(cmd, listCmd) {
		lister, err := e.Handler.ListCourses()
		if err != nil {
			panic(err)
		}
		done := make(chan struct{})
		for course := range lister.Stream(done) {
			e.printCourse(course)
		}
	}
}

func actionCompleter(d prompt.Document) []prompt.Suggest {
	s := []prompt.Suggest{
		{Text: createCmd, Description: "Create a new course"},
		{Text: listCmd, Description: "List existing courses"},
	}
	return prompt.FilterHasPrefix(s, d.GetWordBeforeCursor(), true)
}

func (e *Executor) Completer(d prompt.Document) []prompt.Suggest {
	if d.GetWordBeforeCursor() == createCmd {
		return []prompt.Suggest{}
	}
	return actionCompleter(d)
}

func (e *Executor) printCourse(course models.Course) {
	jsonRepr, err := course.ToJson()
	if err == nil {
		e.out.Write(jsonRepr)
		e.out.Write([]byte{'\n'})
	}
}
