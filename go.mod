module gitlab.com/kyle_anderson/course-dependencies

go 1.17

require (
	// Need to use v0.2.5 until https://github.com/c-bata/go-prompt/issues/228
	//  and https://github.com/c-bata/go-prompt/issues/233 are fixed. */
	github.com/c-bata/go-prompt v0.2.5
	github.com/golang-migrate/migrate/v4 v4.15.1
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.0
)

require github.com/containerd/containerd v1.5.8 // indirect

require (
	github.com/docker/docker v20.10.12+incompatible // indirect
	github.com/hashicorp/errwrap v1.0.0 // indirect
	github.com/hashicorp/go-multierror v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.7 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mattn/go-tty v0.0.3 // indirect
	github.com/opencontainers/image-spec v1.0.2 // indirect
	github.com/pkg/term v1.1.0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/sys v0.0.0-20211025201205-69cdffdb9359 // indirect
	google.golang.org/grpc v1.43.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
