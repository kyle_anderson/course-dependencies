package internal

import "os"

const DATABASE_URL = `DATABASE_URL`

func GetDatabaseUrl() string {
	return os.Getenv(DATABASE_URL)
}
