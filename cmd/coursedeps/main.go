package main

import (
	"fmt"
	"log"
	"os"

	"github.com/c-bata/go-prompt"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/cli"
	"gitlab.com/kyle_anderson/course-dependencies/pkg/database"
	_ "gitlab.com/kyle_anderson/course-dependencies/pkg/database/postgresql"
)

type Basic struct {
	Department, Description string
	Code                    int
}

func main() {
	exitCode := int(0)
	defer func() { os.Exit(exitCode) }()

	handler, err := database.ConnectHandler("postgres", os.Getenv("DATABASE_URL"))
	log.Printf("Handler: %#v. Error: %#v\n", handler, err)
	err = cli.HandleMigrations(handler)
	if err != nil {
		fmt.Println(err)
		exitCode = 1
		return
	}

	fmt.Println("Welcome to the Courses CLI!")
	e := cli.NewExecutor(handler, os.Stdout)
	p := prompt.New(e.Execute, e.Completer)
	p.Run()
}
